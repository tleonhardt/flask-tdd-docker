"""Ping API."""
from flask_restx import Namespace, Resource

ping_namespace = Namespace("ping")


@ping_namespace.route('')
class Ping(Resource):
    """A minimal Flask-RESETX API which responds to a GET request on the /ping endpoint."""

    def get(self):
        """Endpoint health check."""
        return {'status': 'success', 'message': 'pong'}
