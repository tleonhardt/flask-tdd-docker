"""Flask applicaiton using Flask-RESTX.

See the Flask-RESTX Quick start for more info:
  https://flask-restx.readthedocs.io/en/stable/quickstart.html
"""
import os

from flask import Flask
from flask_admin import Admin
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

# instantiate the Flask extensions
db = SQLAlchemy()
admin = Admin(template_mode="bootstrap3")


def create_app(script_info=None):

    # instantiate the app
    app = Flask(__name__)

    # set config
    app_settings = os.getenv('APP_SETTINGS')
    app.config.from_object(app_settings)

    # set up extensions
    db.init_app(app)
    if os.getenv("FLASK_ENV") == "development":
        admin.init_app(app)

    # register api
    from project.api import api

    api.init_app(app)

    # Use Flask-CORS to handle cross-origin requests
    #   e.g., requests that originate from a different protocol, IP address, domain name, or port
    CORS(app)

    # shell context for flask cli
    @app.shell_context_processor
    def ctx():
        return {'app': app, 'db': db}

    return app
