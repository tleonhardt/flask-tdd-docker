# pull official base image based on Alpine to keep our final image slim
FROM python:3.8.1-alpine

# install dependencies
RUN apk update && \
    apk add --virtual build-deps gcc python-dev musl-dev && \
    apk add postgresql-dev && \
    apk add netcat-openbsd

# set environment varibles to prevent Python from writing pyc files to disc and from buffering stdout/stderr
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# set working directory
WORKDIR /usr/src/app

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt .
COPY ./requirements-dev.txt .
RUN pip install -r requirements-dev.txt

# add entrypoint.sh
COPY ./entrypoint.sh .
RUN chmod +x entrypoint.sh

# add app
COPY . .
