# Test-Driven Development with Python, Flask, and Docker

[![pipeline status](https://gitlab.com/tleonhardt/flask-tdd-docker/badges/master/pipeline.svg)](https://gitlab.com/tleonhardt/flask-tdd-docker/commits/master)

This is my code from following along with the course 
[Test-Driven Development with Python, Flask, and Docker](https://testdriven.io/courses/tdd-flask/).
